from django.urls import path
from projects.views import (
    Project_instances,
    Project_detailview,
    Project_createview,
)


urlpatterns = [
    path("", Project_instances.as_view(), name="list_projects"),
    path("<int:pk>/", Project_detailview.as_view(), name="show_project"),
    path("create/", Project_createview.as_view(), name="create_project"),
]
