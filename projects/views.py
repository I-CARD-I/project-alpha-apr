from django.shortcuts import render

# from django.views.generic.list import ListView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


class Project_instances(LoginRequiredMixin, generic.ListView):
    model = Project
    template_name = "projects/projects.html"

    def get(self, request):
        context = {
            "projects": Project.objects.filter(members=self.request.user)
            if Project
            else [],
        }
        return render(request, "projects/projects.html", context)


class Project_detailview(LoginRequiredMixin, generic.DetailView):
    model = Project
    template_name = "projects/detail.html"


class Project_createview(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = [
        "name",
        "description",
        "members",
    ]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
