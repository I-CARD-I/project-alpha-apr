from django.shortcuts import render
from django.views import generic
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy

# Create your views here.
class CreateProjectView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class ListProjectTask(LoginRequiredMixin, generic.ListView):
    model = Task
    template_name = "tasks/mine.html"

    def get(self, request):
        context = {
            "tasks": Task.objects.filter(assignee=self.request.user)
            if Task
            else [],
        }
        return render(request, "tasks/mine.html", context)


class UpdateTaskView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "task/mine.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
