from django.urls import path
from tasks.views import CreateProjectView, ListProjectTask, UpdateTaskView


urlpatterns = [
    path("create/", CreateProjectView.as_view(), name="create_task"),
    path("mine/", ListProjectTask.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", UpdateTaskView.as_view(), name="complete_task"),
]
