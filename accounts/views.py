from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.contrib.auth import login


# Create your views here.
# class RestrictedView(LoginRequiredMixin, TemplateView):


# def signup_view(request):
#    if request.method == "POST":
#        form = UserCreationForm(request.POST)
#        if form.is_valid():
#            form.save()
#
#    form = UserCreationForm()
#    return render(request, "accounts/signup.html", {"form": form})


def signup_view(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password1"]
            user = User.objects.create_user(
                username, email=None, password=password
            )
            user.save()
            if user is not None:
                login(request, user)

            return redirect("home")
    else:
        form = UserCreationForm()

    return render(request, "registration/signup.html", {"form": form})
