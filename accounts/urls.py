from django.urls import path
from django.contrib.auth import views
from accounts.views import signup_view

# from django.contrib.auth import views as auth_views

urlpatterns = [
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.LogoutView.as_view(), name="logout"),
    path("signup/", signup_view, name="signup"),
]
